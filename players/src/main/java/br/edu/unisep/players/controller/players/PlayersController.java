package br.edu.unisep.players.controller.players;

import br.edu.unisep.players.domain.dto.players.PlayersDto;
import br.edu.unisep.players.domain.dto.players.RegisterPlayerDto;
import br.edu.unisep.players.domain.usecase.players.FindPlayersByOtherUsersUseCase;
import br.edu.unisep.players.domain.usecase.players.FindPlayersByUserUseCase;
import br.edu.unisep.players.domain.usecase.players.RegisterPlayersUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/players")
@AllArgsConstructor
public class PlayersController {

    private final FindPlayersByUserUseCase findPlayersByUserUseCase;
    private final FindPlayersByOtherUsersUseCase findPlayersByOtherUsersUseCase;
    private final RegisterPlayersUseCase registerPlayersUseCase;

    @RolesAllowed({"ROLE_ADMIN" , "ROLE_CLIENT"})
    @GetMapping("/my-players")
    public ResponseEntity<List<PlayersDto>> findMyPlayers(@RequestAttribute("tokenUserId") Integer userId) {
        var players = findPlayersByUserUseCase.execute(userId);
        return ResponseEntity.ok(players);
    }

    @RolesAllowed({"ROLE_ADMIN" , "ROLE_CLIENT"})
    @GetMapping("/{userId}")
    public ResponseEntity<List<PlayersDto>> findByUser(@PathVariable("userId") Integer userId) {
        var players = findPlayersByUserUseCase.execute(userId);
        return ResponseEntity.ok(players);
    }

    @RolesAllowed("ROLE_ADMIN")
    @GetMapping("/by-others")
    public ResponseEntity<List<PlayersDto>> findByOtherUsers(@RequestAttribute("tokenUserId") Integer userId) {
        var players = findPlayersByOtherUsersUseCase.execute(userId);
        return ResponseEntity.ok(players);
    }

    @PermitAll
    @PostMapping
    public ResponseEntity save(@RequestAttribute("tokenUserId") Integer userId,
                               @RequestBody RegisterPlayerDto registerPlayers) {
        registerPlayersUseCase.execute(registerPlayers, userId);
        return ResponseEntity.ok().build();
    }
}
