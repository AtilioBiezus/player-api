package br.edu.unisep.players.controller.teams;

import br.edu.unisep.players.domain.dto.teams.TeamsDto;
import br.edu.unisep.players.domain.usecase.teams.FindAllTeamsUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.List;

@PermitAll
@RestController
@RequestMapping("/teams")
@AllArgsConstructor
public class TeamsController {

    private final FindAllTeamsUseCase findAllTeamsUseCase;

    @RolesAllowed({"ROLE_ADMIN"})
    @GetMapping
    public ResponseEntity<List<TeamsDto>> findAll() {
        var teams = findAllTeamsUseCase.execute();
        return ResponseEntity.ok(teams);
    }
}
