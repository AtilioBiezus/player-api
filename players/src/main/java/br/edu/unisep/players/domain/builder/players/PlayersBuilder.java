package br.edu.unisep.players.domain.builder.players;

import br.edu.unisep.base.data.entity.User;
import br.edu.unisep.players.data.entity.players.Players;
import br.edu.unisep.players.data.entity.teams.Teams;
import br.edu.unisep.players.domain.builder.teams.TeamsBuilder;
import br.edu.unisep.players.domain.dto.players.PlayersDto;
import br.edu.unisep.players.domain.dto.players.RegisterPlayerDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class PlayersBuilder {

    private final TeamsBuilder teamBuilder;

    public List<PlayersDto> from(List<Players> players) {
        return players.stream().map(this::from).collect(Collectors.toList());
    }

    public PlayersDto from(Players players) {
        return new PlayersDto(
                players.getId(),
                players.getName(),
                players.getNationality(),
                players.getPrice(),
                players.getUser().getName(),
                teamBuilder.from(players.getTeams())
        );
    }
    public Players from(RegisterPlayerDto registerPlayers, Integer userId) {
        var players = new Players();
        players.setName(registerPlayers.getName());
        players.setNationality(registerPlayers.getNationality());
        players.setPrice(registerPlayers.getPrice());

        var teams = new Teams();
        teams.setId(registerPlayers.getTeams());
        players.setTeams(teams);

        var user = new User();
        user.setId(userId);
        players.setUser(user);

        return players;
    }
}
