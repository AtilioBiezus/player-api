package br.edu.unisep.players.domain.dto.teams;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TeamsDto {

    private final Integer id;

    private final String name;
}
