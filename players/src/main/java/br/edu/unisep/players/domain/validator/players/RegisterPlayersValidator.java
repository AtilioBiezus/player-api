package br.edu.unisep.players.domain.validator.players;

import org.springframework.stereotype.Component;

import br.edu.unisep.players.domain.dto.players.RegisterPlayerDto;
import static br.edu.unisep.players.domain.validator.ValidationMessages.*;
import static org.apache.commons.lang3.Validate.*;

@Component
public class RegisterPlayersValidator {

    public void validate(RegisterPlayerDto registerPlayers) {
        notEmpty(registerPlayers.getName(), MESSAGE_INVALID_NAME);
        notEmpty(registerPlayers.getNationality(), MESSAGE_INVALID_NATIONALITY);

        notNull(registerPlayers.getPrice(), MESSAGE_INVALID_PRICE);
        isTrue(registerPlayers.getPrice() > 0d, MESSAGE_INVALID_PRICE);

        notNull(registerPlayers.getTeams(), MESSAGE_INVALID_TEAM);
    }
}
