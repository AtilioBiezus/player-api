package br.edu.unisep.players.domain.dto.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseErrorDto {

    private final String message;

}
