package br.edu.unisep.players.domain.builder.teams;

import br.edu.unisep.players.data.entity.teams.Teams;
import br.edu.unisep.players.domain.dto.teams.TeamsDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamsBuilder {


    public List<TeamsDto> from(List<Teams> teams) {
        return teams.stream().map(this::from).collect(Collectors.toList());
    }

    public TeamsDto from(Teams teams) {
        return new TeamsDto(
                teams.getId(),
                teams.getName()
        );
    }
}
