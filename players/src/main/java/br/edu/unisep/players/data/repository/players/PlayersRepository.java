package br.edu.unisep.players.data.repository.players;

import br.edu.unisep.players.data.entity.players.Players;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface PlayersRepository extends JpaRepository<Players, Integer> {

    @Query("from Players where user.id = :userId")
    List<Players> findByUser(Integer userId);

    @Query("from Players where user.id <> :userId")
    List<Players> findByOtherUsers(Integer userId);

}
