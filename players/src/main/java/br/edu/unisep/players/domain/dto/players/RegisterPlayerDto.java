package br.edu.unisep.players.domain.dto.players;

import lombok.Data;

@Data
public class RegisterPlayerDto {

    private final String name;
    private final String nationality;

    private final Double price;
    private final Integer teams;
}

