package br.edu.unisep.players.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationMessages {

    public static final String MESSAGE_INVALID_USER_ID = "Usuário inválido!";

    public static final String MESSAGE_INVALID_NAME = "Nome inválido!";
    public static final String MESSAGE_INVALID_NATIONALITY = "Nacionalidade inválida!";
    public static final String MESSAGE_INVALID_PRICE = "Preço inválido!";
    public static final String MESSAGE_INVALID_TEAM = "Time inválido!";
}

