package br.edu.unisep.players.data.repository.teams;

import br.edu.unisep.players.data.entity.teams.Teams;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TeamsRepository extends JpaRepository<Teams, Integer> {
}
