package br.edu.unisep.players.domain.usecase.players;


import br.edu.unisep.players.domain.builder.players.PlayersBuilder;
import br.edu.unisep.players.domain.validator.user.UserIdValidator;
import br.edu.unisep.players.data.repository.players.PlayersRepository;
import br.edu.unisep.players.domain.dto.players.PlayersDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindPlayersByUserUseCase {

    private final UserIdValidator userIdValidator;
    private final PlayersRepository playersRepository;
    private final PlayersBuilder playersBuilder;

    public List<PlayersDto> execute(Integer userId) {
        userIdValidator.validate(userId);

        var players = playersRepository.findByUser(userId);
        return playersBuilder.from(players);
    }

}
