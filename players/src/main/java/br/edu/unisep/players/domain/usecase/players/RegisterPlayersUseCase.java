package br.edu.unisep.players.domain.usecase.players;

import br.edu.unisep.players.data.repository.players.PlayersRepository;
import br.edu.unisep.players.domain.builder.players.PlayersBuilder;
import br.edu.unisep.players.domain.dto.players.RegisterPlayerDto;
import br.edu.unisep.players.domain.validator.players.RegisterPlayersValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterPlayersUseCase {

    private final RegisterPlayersValidator registerPlayersValidator;
    private final PlayersRepository playersRepository;
    private final PlayersBuilder playersBuilder;

    public void execute(RegisterPlayerDto registerPlayer, Integer userId) {
        registerPlayersValidator.validate(registerPlayer);

        var players = playersBuilder.from(registerPlayer, userId);
        playersRepository.save(players);
    }
}
