package br.edu.unisep.players.domain.validator.user;

import org.springframework.stereotype.Component;

import static br.edu.unisep.players.domain.validator.ValidationMessages.MESSAGE_INVALID_USER_ID;
import static org.apache.commons.lang3.Validate.notNull;

@Component
public class UserIdValidator {

    public void validate(Integer userId) {
        notNull(userId, MESSAGE_INVALID_USER_ID);
    }

}