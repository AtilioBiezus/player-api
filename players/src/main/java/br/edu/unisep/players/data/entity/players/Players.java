package br.edu.unisep.players.data.entity.players;

import br.edu.unisep.players.data.entity.teams.Teams;
import lombok.Data;
import br.edu.unisep.base.data.entity.User;

import javax.persistence.*;


@Data
@Entity
@Table(name = "players")
public class Players {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "nationality")
    private String nationality;

    @OneToOne
    @JoinColumn(name = "teams_id")
    private Teams teams;

    @Column(name = "price")
    private Double price;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
