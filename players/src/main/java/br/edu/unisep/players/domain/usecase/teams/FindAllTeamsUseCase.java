package br.edu.unisep.players.domain.usecase.teams;

import br.edu.unisep.players.data.repository.teams.TeamsRepository;
import br.edu.unisep.players.domain.builder.teams.TeamsBuilder;
import br.edu.unisep.players.domain.dto.teams.TeamsDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllTeamsUseCase {

    private final TeamsRepository teamsRepository;
    private final TeamsBuilder teamsBuilder;

    public List<TeamsDto> execute() {
        var teams = teamsRepository.findAll();
        return teamsBuilder.from(teams);
    }


}

