package br.edu.unisep.players.domain.dto.players;

import br.edu.unisep.players.domain.dto.teams.TeamsDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PlayersDto {

    private final Integer id;

    private final String name;
    private final String nationality;
    private final Double price;
    private final String user;
    private final TeamsDto team;
}
